FROM alpine:3.5

# Maintainer
MAINTAINER Victor Suarez<victor.suarez.ferrer@gmail.com>

ARG glassfish_version

ENV         TZ Europe/Paris
ENV         GLASSFISH_VERSION    4.1.1
ENV         GLASSFISH_HOME    /usr/local/glassfish4
ENV         GLASSFISH_URL http://download.java.net/glassfish/${GLASSFISH_VERSION}/release/glassfish-${GLASSFISH_VERSION}.zip
ENV         TMP_ZIP_FILE glassfish-${GLASSFISH_VERSION}.zip
ENV         MAVEN_VERSION="3.2.5"
ENV         M2_HOME=/usr/lib/mvn

#  openjdk8-jre-lib-8.111.14-r0:
ENV JAVA_VERSION_MAJOR 8
ENV JAVA_VERSION_MINOR 111
ENV JAVA_VERSION_BUILD 14
ENV JAVA_VERSION_REVIS r1

#install Open-JDK 8
RUN echo '@edge http://nl.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories \
 && echo '@community http://nl.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories \
 && apk update && apk upgrade \
 && apk add openjdk8-jre-lib@community=${JAVA_VERSION_MAJOR}.${JAVA_VERSION_MINOR}.${JAVA_VERSION_BUILD}-${JAVA_VERSION_REVIS} \
 && apk add openjdk8-jre-base@community=${JAVA_VERSION_MAJOR}.${JAVA_VERSION_MINOR}.${JAVA_VERSION_BUILD}-${JAVA_VERSION_REVIS} \
 && apk add openjdk8-jre@community=${JAVA_VERSION_MAJOR}.${JAVA_VERSION_MINOR}.${JAVA_VERSION_BUILD}-${JAVA_VERSION_REVIS} \
 && apk add openjdk8@community=${JAVA_VERSION_MAJOR}.${JAVA_VERSION_MINOR}.${JAVA_VERSION_BUILD}-${JAVA_VERSION_REVIS} \
 && rm -rf /var/cache/apk/*

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk

#install wget
RUN apk add --update wget

#Install Maven
RUN apk add --update wget && \
  cd /tmp && \
  wget "http://ftp.unicamp.br/pub/apache/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz" && \
  tar -zxvf "apache-maven-$MAVEN_VERSION-bin.tar.gz" && \
  mv "apache-maven-$MAVEN_VERSION" "$M2_HOME" && \
  ln -s "$M2_HOME/bin/mvn" /usr/bin/mvn && \
  rm /tmp/* /var/cache/apk/*

# Install packages, download and extract GlassFish
RUN    wget -O /tmp/$TMP_ZIP_FILE $GLASSFISH_URL && \
            unzip /tmp/$TMP_ZIP_FILE -d /usr/local && \
            rm -f /tmp/$TMP_ZIP_FILE

#Add Glassfish to the path
ENV         PATH              $PATH:$GLASSFISH_HOME/bin:${JAVA_HOME}/bin

EXPOSE      8080 4848 8181

# verbose causes the process to remain in the foreground so that docker can track it
CMD         asadmin start-domain --verbose
